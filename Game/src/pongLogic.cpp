#include "pongLogic.hpp"

Position::Position(float x, float y) : x(x), y(y) {
    index = 0;
}

Velocity::Velocity(float x, float y, float speed) : x(x), y(y), speed(speed) {
    index = 1;
}

Player::Player(int playerNbr) : playerNbr(playerNbr) {
    index = 2;
}

RenderComponent::RenderComponent(sf::Shape* shape) :  shape(shape) {
    index = 3;
}

Ball::Ball(sf::Shape* shape) : shape(shape) {
    index = 4;
}

PlayerBox::PlayerBox(sf::Shape* shape) : shape(shape) {
    index = 5;
}

BallBounds::BallBounds(float left, float right) : left(left), right(right) {
    index = 6;
}


MoveSystem::MoveSystem() {
    componentMask.set(0);//position
    componentMask.set(1);//Velocity
}

void MoveSystem::update(ECS1::EntityManager *em, float deltaTime) {
    std::vector<ECS1::Entity*> entities = em->entitiesWithComponents(componentMask);

    Position* p;
    Velocity* v;
    for(ECS1::Entity* e : entities) {
        p = e->getComponent<Position>();
        v = e->getComponent<Velocity>();

        p->x += (v->x * deltaTime * v->speed);
        p->y += (v->y * deltaTime * v->speed);
    }
}

PlayerMoveSystem::PlayerMoveSystem() {
    componentMask.set(0); //Position
    componentMask.set(1); //Velocity
    componentMask.set(2); //player
}

void PlayerMoveSystem::update(ECS1::EntityManager *em, float deltaTime) {
    std::vector<ECS1::Entity*> entities = em->entitiesWithComponents(componentMask);

    Position* p;
    Velocity *v;
    Player* player;
    for(ECS1::Entity* e : entities) {
        p = e->getComponent<Position>();
        v = e->getComponent<Velocity>();
        player = e->getComponent<Player>();

        sf::Keyboard::Key up;
        sf::Keyboard::Key down;
        //Player one
        if(player->playerNbr == 1) {
            up = sf::Keyboard::Up;
            down = sf::Keyboard::Down;
        }
        // Player two
        else {
            up = sf::Keyboard::W;
            down = sf::Keyboard::S;
        }

        if(sf::Keyboard::isKeyPressed(up)) {
            if(p->y > UPPER_BOUNDARY) {
                v->y = -1;
            }
            else {
                v->y = 0;
            }
        }
        else if(sf::Keyboard::isKeyPressed(down)) {
            if(p->y < LOWER_BOUNDARY) {
                v->y = 1;
            }
            else {
                v->y = 0;
            }
        }
        else {
            v->y = 0;
        }
    }
}

RenderSystem::RenderSystem(sf::RenderWindow* window) : window(window) {
    componentMask.set(0); //Position
    componentMask.set(3); //RenderComponent
}

void RenderSystem::update(ECS1::EntityManager *em, float deltaTime) {
    std::vector<ECS1::Entity*> entities = em->entitiesWithComponents(componentMask);

    RenderComponent* r;
    Position* p;
    for(ECS1::Entity* e : entities) {
        r = e->getComponent<RenderComponent>();
        p = e->getComponent<Position>();
        r->shape->setPosition(p->x, p->y);
        window->draw(*r->shape);
    }
}

BallBounceSystem::BallBounceSystem() {
    componentMask.set(0); //Position
    componentMask.set(1); //Velocity
    componentMask.set(4); //Ball

    playerMask.set(0); //Position
    playerMask.set(5); //PlayerBox
}

void BallBounceSystem::update(ECS1::EntityManager *em, float deltaTime) {
    ECS1::Entity* ball = em->entitiesWithComponents(componentMask).at(0);
    std::vector<ECS1::Entity*> players = em->entitiesWithComponents(playerMask);

    Ball* ball_comp = ball->getComponent<Ball>();
    Velocity* ball_vel = ball->getComponent<Velocity>();
    Position* ball_pos = ball->getComponent<Position>();

    PlayerBox* player_box;
    Position* player_pos;

    for(ECS1::Entity* e : players) {
       player_pos = e->getComponent<Position>();
       player_box = e->getComponent<PlayerBox>();

       if(ball_comp->shape->getGlobalBounds().intersects(player_box->shape->getGlobalBounds())) {

           float distance_from_middle =  (player_pos->y + 50) - ball_pos->y; // Hardcoded and should be changed

           std::cout << "ball pos: " << ball_pos->x << "," << ball_pos->y << "\n";
           std::cout << "player pos: " << player_pos->x << "," << player_pos->y + 50 << "\n";
           std::cout << "distance: " << distance_from_middle << "\n";

           if(ball_vel->x > 0) {
               ball_vel->x = -cos((distance_from_middle*3.1415926535897)/180);
           }
           else {
               ball_vel->x = cos((distance_from_middle*3.1415926535897)/180);
           }
           ball_vel->y =  -sin((distance_from_middle*3.1415926535897)/180);

           ball_pos->x = ball_pos->x + (ball_vel->x * 10); //Hard coded buff to ensure we leave

           std::cout << "new velocity: " << ball_vel->x << ", " << ball_vel->y << "\n";
       }
    }
}

BallBoundarySystem::BallBoundarySystem() {
    componentMask.set(6); //ball boundary
}

void BallBoundarySystem::update(ECS1::EntityManager *em, float deltaTime) {
    ECS1::Entity* ball = em->entitiesWithComponents(componentMask).at(0);

    Position* pos = ball->getComponent<Position>();
    Velocity* vel = ball->getComponent<Velocity>();
    BallBounds* boundary = ball->getComponent<BallBounds>();

    if(pos->y < UPPER_BOUNDARY || pos->y > LOWER_BOUNDARY + 50) {
        vel->y = -vel->y;
    }

    if(pos->x < boundary->left || pos->x > boundary -> right) {
        vel->x = 1;
        vel->y = 0;
        pos->x = (boundary->right - boundary->left)/2;
        pos->y = (LOWER_BOUNDARY + 50 - UPPER_BOUNDARY)/2;
    }
}
