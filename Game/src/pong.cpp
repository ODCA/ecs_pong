#include <iostream>
#include <ECS1/ECS1.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "pongLogic.hpp"

//set up all systems
ECS1::SystemManager* setUpSystems(ECS1::EntityManager* em, sf::RenderWindow* window) {
    ECS1::SystemManager* sm = new ECS1::SystemManager(em);
    sm->addSystem<MoveSystem>();
    sm->addSystem<PlayerMoveSystem>();
    sm->addSystem<RenderSystem>(window);
    sm->addSystem<BallBounceSystem>();
    sm->addSystem<BallBoundarySystem>();
    return sm;
}

//create initial entities
ECS1::EntityManager* setUpEntities(sf::RenderWindow* window) {
    ECS1::EntityManager* em = new ECS1::EntityManager();

    //create player 1
    ECS1::Entity* player1 = em->createEntity();
    player1->assignComponent<Position>(
        ((float)window->getSize().x - 30),
        ((float)window->getSize().y/2 - 50)
    );
    player1->assignComponent<Velocity>(0.0f,0.0f, 300.0f);
    player1->assignComponent<Player>(1);
    sf::Shape* shape = new sf::RectangleShape(sf::Vector2f(20.0f, 100.0f));
    shape->setFillColor(sf::Color::White);
    player1->assignComponent<RenderComponent>(shape);
    player1->assignComponent<PlayerBox>(shape);

    //create player 2
    ECS1::Entity* player2 = em->createEntity();
    player2->assignComponent<Position>(
        30,
        ((float)window->getSize().y/2 - 50)
    );
    player2->assignComponent<Velocity>(0.0f, 0.0f, 300.0f);
    player2->assignComponent<Player>(2);
    shape = new sf::RectangleShape(sf::Vector2f(20.0f, 100.0f));
    shape->setFillColor(sf::Color::White);
    player2->assignComponent<RenderComponent>(shape);
    player2->assignComponent<PlayerBox>(shape);

    //create ball
    ECS1::Entity* ball = em->createEntity();
    ball->assignComponent<Position>(
        ((float)window->getSize().x/2),
        ((float)window->getSize().y/2)
    );
    ball->assignComponent<Velocity>(1.0f, 0.0f, 500.0f);
    shape = new sf::CircleShape(10);
    shape->setFillColor(sf::Color::White);
    ball->assignComponent<RenderComponent>(shape);
    ball->assignComponent<Ball>(shape);
    ball->assignComponent<BallBounds>(0.0f, ((float)window->getSize().x));
    return em;
}

int main(int argc, char* argv[])
{
    // Create window
    sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(1200,800), "SFML works");

    // create time
    sf::Time deltaTime;
    sf::Clock clock;
    clock.restart();

    //setup game
    ECS1::EntityManager* em = setUpEntities(window);
    ECS1::SystemManager* sm = setUpSystems(em, window);


    window->setFramerateLimit(100);
    //gameloop
    while(window->isOpen()){
        sf::Time elapsed_time = clock.getElapsedTime();
        clock.restart();
        sf::Event event;
        while(window->pollEvent(event)) {
            if(event.type == sf::Event::Closed) {
                window->close();
            }
        }

        window->clear();
        sm->update(elapsed_time.asSeconds());
        window->display();
    }
    std::cout << "games is over :(" << "\n";
    return 0;
}
