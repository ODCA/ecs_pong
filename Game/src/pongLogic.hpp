#include <ECS1/ECS1.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <math.h>

#define UPPER_BOUNDARY 30
#define LOWER_BOUNDARY 670

struct Position : public ECS1::Component {
    float x,y;
    Position(float x, float y);
};

struct Velocity : public ECS1::Component {
    float x,y;
    float speed;
    Velocity(float x, float y, float speed);
};

struct Player : public ECS1::Component {
    int playerNbr;
    Player(int playernbr);
};


struct RenderComponent : public ECS1::Component {
    sf::Shape* shape;

    RenderComponent(sf::Shape* shape);
};

struct Ball : public ECS1::Component {
    sf::Shape* shape;
    Ball(sf::Shape* shape);
};

struct PlayerBox : public ECS1::Component {
    sf::Shape* shape;
    PlayerBox(sf::Shape* shape);
};

struct BallBounds : public ECS1::Component {
    float left, right;
    BallBounds(float left, float right);
};


class MoveSystem : public ECS1::System {
public:
    MoveSystem();
    void update(ECS1::EntityManager *em, float deltaTime) override;
};

class PlayerMoveSystem : public ECS1::System {
public:
    PlayerMoveSystem();
    void update(ECS1::EntityManager * em, float deltaTime) override;
};

class RenderSystem : public ECS1::System {
private:
    sf::RenderWindow* window;
public:
    RenderSystem(sf::RenderWindow* window);
    void update(ECS1::EntityManager *em, float deltaTime) override;
};

class BallBounceSystem : public ECS1::System {
private:
    std::bitset<128> playerMask;

public:
    BallBounceSystem();
    void update(ECS1::EntityManager *em, float deltaTime) override;

};

class BallBoundarySystem : public ECS1::System {
public:
    BallBoundarySystem();
    void update(ECS1::EntityManager *em, float deltaTime) override;
};
